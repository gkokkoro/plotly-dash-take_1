from plotly_dash_take_1 import init_app

app = init_app()

if __name__ == "__main__":
    app.run(debug = True)
