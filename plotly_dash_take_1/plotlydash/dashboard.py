import dash

def init_dashboard(server):
    dash_app = dash.Dash(server = server, routes_pathname_prefix ="/dashapp", external_stylesheets=['/static/dist/css/styles.css',])

    dash_app.layout = html.Div(id = 'dash-container')
    return(dash_app.server)
