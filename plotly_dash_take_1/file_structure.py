import os
import copy

def create_file_structure(starting_file):
    file_structure = []

    files = os.listdir(starting_file)
    for file in files:
        file_info = {}
        file_info["name"] = file
        if os.path.isdir(starting_file+file):
            file_info["type"] = "dir"
            file_info["contents"] = create_file_structure(starting_file+file+"/")
        else:
            file_info["type"] = "file"
            contents = starting_file + file
            split_contents = contents.split("/",2)
            file_info["contents"] = split_contents[0] + "/" + split_contents[2]
        file_structure.append(copy.deepcopy(file_info))
    return(file_structure)


if __name__ == "__main__":
    print()
    print(create_file_structure('../data/'))
