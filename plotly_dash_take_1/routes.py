"""Routes for parent Flask app."""
import os
from flask import render_template, redirect
from flask import current_app as app
import numpy as np
from plotly_dash_take_1 import file_structure as fs

@app.route('/')
def re_to_home():
    return redirect('/data')

@app.route('/data/')
@app.route('/data/<path:dir_name>')
def home(dir_name = ""):
    """Landing page."""
    return_string = ""
    path_name = './data/'+dir_name
    if os.path.isdir(path_name):
        if dir_name != '':
            dir_name += "/"
        file_structure = fs.create_file_structure('./data/'+dir_name)

        return(render_template("tree_dir_explorer.html", nav=file_structure))
    elif os.path.isfile(path_name):
        data = np.genfromtxt(path_name, delimiter=',', dtype=int)
        return(str(data))

    return(return_string)
